Source: networking-mlnx
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Lenny Verkhovsky <lennyb@mellanox.com>,
 Sakirnth Nagarasa <sakirnth@gmail.com>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 po-debconf,
 python3-all,
 python3-pbr,
 python3-sphinx,
Build-Depends-Indep:
 python3-babel,
 python3-cliff,
 python3-coverage,
 python3-ddt,
 python3-defusedxml,
 python3-ethtool,
 python3-eventlet,
 python3-fixtures,
 python3-hacking,
 python3-mock,
 python3-netaddr,
 python3-neutron (>= 2:16.0.0),
 python3-neutron-lib (>= 1.28.0),
 python3-neutronclient,
 python3-openstackclient,
 python3-os-testr,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.privsep,
 python3-oslosphinx,
 python3-oslotest,
 python3-psycopg2,
 python3-pymysql,
 python3-requests-mock,
 python3-six,
 python3-sqlalchemy,
 python3-testresources,
 python3-testscenarios,
 python3-testtools,
 python3-webtest,
 python3-zmq,
 subunit,
 testrepository,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/openstack-team/neutron-plugins/networking-mlnx
Vcs-Git: https://salsa.debian.org/openstack-team/neutron-plugins/networking-mlnx.git
Homepage: https://opendev.org/x/networking-mlnx.git

Package: networking-mlnx-common
Architecture: all
Depends:
 neutron-common  (>= 1:15.0.0),
 python3-networking-mlnx (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack virtual network service - Mellanox plugin common files
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 This package provides the Mellanox plugin common files.

Package: networking-mlnx-eswitchd
Architecture: all
Provides:
 neutron-plugin,
Depends:
 networking-mlnx-common (= ${binary:Version}),
 ${misc:Depends},
 ${ostack-lsb-base},
 ${python3:Depends},
Description: Neutron is a virtual network service for Openstack - Mellanox eswitchd
 Neutron is a virtual network service for Openstack, and a part of
 Netstack. Just like OpenStack Nova provides an API to dynamically
 request and configure virtual servers, Neutron provides an API to
 dynamically request and configure virtual networks. These networks
 connect "interfaces" from other OpenStack services (e.g., virtual NICs
 from Nova VMs). The Neutron API supports extensions to provide
 advanced network capabilities (e.g., QoS, ACLs, network monitoring,
 etc.)
 .
 This package provides the Mellanox eswitchd.

Package: neutron-mlnx-agent
Architecture: all
Section: python
Depends:
 networking-mlnx-common (= ${binary:Version}),
 ${misc:Depends},
 ${ostack-lsb-base},
 ${python3:Depends},
Recommends:
 iputils-arping,
Description: OpenStack virtual network service - Mellanox plugin agent
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 This package provides the Mellanox plugin agent.

Package: python3-networking-mlnx
Architecture: all
Section: python
Depends:
 python3-babel,
 python3-defusedxml,
 python3-ethtool,
 python3-eventlet,
 python3-netaddr,
 python3-neutron (>= 2:16.0.0),
 python3-neutron-lib (>= 1.28.0),
 python3-neutronclient,
 python3-openstackclient,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.privsep,
 python3-pbr,
 python3-six,
 python3-sqlalchemy,
 python3-zmq,
 ${misc:Depends},
 ${python3:Depends},
Breaks:
 python-networking-mlnx,
Replaces:
 python-networking-mlnx,
Description: OpenStack virtual network service - Mellanox plugin Python 2.7 files
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 This package provides the Python 2.7 files for the Mellanox plugin.
